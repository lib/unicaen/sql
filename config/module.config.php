<?php

namespace UnicaenSql;

use Unicaen\Console\Router\Simple;
use UnicaenSql\Controller\ConsoleController;
use UnicaenSql\Controller\ConsoleControllerFactory;
use UnicaenSql\Service\SQL\RunSQLService;
use UnicaenSql\Service\SQL\RunSQLServiceFactory;

return [
    'bjyauthorize' => [
        'guards' => [
            'BjyAuthorize\Guard\Controller' => [
                ['controller' => ConsoleController::class, 'action' => 'runSQLScript', 'roles' => []],
                ['controller' => ConsoleController::class, 'action' => 'runSQLQuery', 'roles' => []],
            ],
        ],
    ],
    'console' => [
        'router' => [
            'routes' => [
                'run-sql-script' => [
                    'type' => Simple::class,
                    'options' => [
                        'route' => 'run-sql-script --path= [--logfile=] [--connection=]',
                        'defaults' => [
                            /** @see \UnicaenSql\Controller\ConsoleController::runSQLScriptAction() */
                            'controller' => ConsoleController::class,
                            'action' => 'runSQLScript',
                        ],
                    ],
                ],
                'run-sql-query' => [
                    'type' => Simple::class,
                    'options' => [
                        'route' => 'run-sql-query --sql= [--logfile=] [--connection=]',
                        'defaults' => [
                            /** @see \UnicaenSql\Controller\ConsoleController::runSQLQueryAction() */
                            'controller' => ConsoleController::class,
                            'action' => 'runSQLQuery',
                        ],
                    ],
                ],
            ],
        ],
        'view_manager' => [
            'display_not_found_reason' => true,
            'display_exceptions' => true,
        ],
    ],
    'controllers' => [
        'factories' => [
            ConsoleController::class => ConsoleControllerFactory::class,
        ],
    ],
    'service_manager' => [
        'factories' => [
            RunSQLService::class => RunSQLServiceFactory::class,
        ],
    ],
];