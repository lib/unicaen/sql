<?php

namespace UnicaenSql\Service\SQL;

use Doctrine\DBAL\Exception;
use Doctrine\DBAL\Logging\DebugStack;

class RunSQLQueryStack extends DebugStack
{
    public function stopQueryWithException(Exception $exception): void
    {
        $this->stopQuery();

        if ($this->enabled) {
            $this->queries[$this->currentQuery]['exception'] = $exception;
        }
    }

    /**
     * @return array
     */
    public function getQueries(): array
    {
        return $this->queries;
    }

    /**
     * @return bool
     */
    public function lastQueryHasException(): bool
    {
        return isset($this->queries[$this->currentQuery]['exception']);
    }

}