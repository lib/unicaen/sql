<?php

namespace UnicaenSql\Service\SQL;

use Exception;
use Laminas\Log\Formatter\Simple;
use Laminas\Log\Logger;
use Laminas\Log\LoggerInterface;
use Laminas\Log\Writer\Stream;
use Laminas\Log\Writer\WriterInterface;
use LogicException;
use RuntimeException;

/**
 * Classe permettant de représenter le résultat de l'exécution d'un processus inconnu quelconque.
 *
 * @see ProcessResultInterface
 *
 * @author Unicaen
 */
abstract class ProcessResult implements ProcessResultInterface
{
    /**
     * @var resource
     */
    private $logStream;

    /**
     * @var WriterInterface
     */
    private $logWriter;

    /**
     * @var bool
     */
    private $success;

    /**
     * @var Exception
     */
    private $exception;

    /**
     * @var float
     */
    private $startMicrotime;

    /**
     * @var float
     */
    private $endMicrotime;

    public function __construct()
    {
        $format = '%message%'; // '%timestamp% %priorityName% (%priority%): %message%' . PHP_EOL;
        $formatter = new Simple($format);

        $this->logStream = fopen('php://memory','r+');

        $this->logWriter = new Stream($this->logStream);
        $this->logWriter->setFormatter($formatter);

        $this->setStartMicrotime();
    }

    public function __destruct()
    {
        // Test ajouté pour éviter l'étrange "Warning: fclose(): supplied resource is not a valid stream resource"
        if (is_resource($this->logStream)) {
            fclose($this->logStream);
        }
    }

    /**
     * Attache un logger pour stocker les éventuels logs qu'il génèrera.
     *
     * Concrètement, cela ajoute au logger spécifié 'writer' permettant qui stockera les logs
     * pour les restituer ultèrieurement via la méthode {@see getLog()}.
     *
     * @param LoggerInterface $logger
     * @return self
     */
    public function attachLogger(LoggerInterface $logger)
    {
        if (! $logger instanceof Logger) {
            throw new LogicException("Logger spécifié non supporté, désolé!");
        }

        $logger->addWriter($this->logWriter);

        return $this;
    }

    /**
     * Retourne les logs stockés.
     *
     * @return string
     */
    public function getLog()
    {
        $offset = ftell($this->logStream);

        rewind($this->logStream);
        $logs = stream_get_contents($this->logStream);
        fseek($this->logStream, $offset);

        return $logs;
    }

    /**
     * Retourne le booléen indiquant si l'exécution est couronnée de succès ou non.
     *
     * @return bool
     */
    public function isSuccess()
    {
        return $this->success;
    }

    /**
     * Positionne le booléen indiquant si l'exécution est couronnée de succès ou non.
     *
     * @param bool $success
     * @return self
     */
    public function setIsSuccess($success = true)
    {
        $this->success = (bool) $success;

        return $this;
    }

    /**
     * @return float
     */
    public function getStartMicrotime()
    {
        return $this->startMicrotime;
    }

    /**
     * @param float|null $startMicrotime
     */
    public function setStartMicrotime($startMicrotime = null)
    {
        $this->startMicrotime = $startMicrotime ?: microtime(true);
    }

    /**
     * @return float
     */
    public function getEndMicrotime()
    {
        return $this->endMicrotime;
    }

    /**
     * @param float|null $endMicrotime
     */
    public function setEndMicrotime($endMicrotime = null)
    {
        $this->endMicrotime = $endMicrotime ?: microtime(true);
    }

    /**
     * @return float
     */
    public function getDurationInSec()
    {
        $startMicrotime = $this->getStartMicrotime();
        $endMicrotime = $this->getEndMicrotime();

        if ($startMicrotime === null || $endMicrotime === null) {
            throw new RuntimeException("Impossible de calculer la durée car l'instant de début ou de fin est null !");
        }

        return $endMicrotime - $startMicrotime;
    }

    /**
     * Retourne l'éventuelle exception rencontrée lors de l'exécution.
     *
     * @return Exception
     */
    public function getException()
    {
        return $this->exception;
    }

    /**
     * Renseigne l'exception rencontrée lors de l'exécution.
     *
     * @param Exception $exception
     * @return self
     */
    public function setException(Exception $exception)
    {
        $this->exception = $exception;

        return $this;
    }
}
