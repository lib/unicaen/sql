<?php

namespace UnicaenSql\Service\SQL;

use Laminas\Log\Logger;
use Laminas\Log\LoggerInterface;
use Laminas\Log\Writer\Noop;
use Psr\Container\ContainerInterface;

class RunSQLServiceFactory
{
    public function __invoke(ContainerInterface $container): RunSQLService
    {
        $service = new RunSQLService();
        $service->setLogger($this->createLogger());

        return $service;
    }

    private function createLogger(): LoggerInterface
    {
        $logger = new Logger();
        $logger->addWriter(new Noop());

        return $logger;
    }
}