<?php

namespace UnicaenSql\Service\SQL;

/**
 * Résultat de l'exécution d'instructions SQL.
 *
 * @author Unicaen
 */
class RunSQLResult extends ProcessResult
{
    /**
     * @var string
     */
    protected $scriptPath;

    /**
     * @var string
     */
    protected $logFilePath;

    /**
     * @return string
     */
    public function getScriptPath()
    {
        return $this->scriptPath;
    }

    /**
     * @param string $scriptPath
     * @return self
     */
    public function setScriptPath($scriptPath)
    {
        $this->scriptPath = $scriptPath;
        return $this;
    }

    /**
     * @return string
     */
    public function getLogFilePath()
    {
        return $this->logFilePath;
    }

    /**
     * @param string $logFilePath
     * @return self
     */
    public function setLogFilePath($logFilePath)
    {
        $this->logFilePath = $logFilePath;
        return $this;
    }
}