<?php

namespace UnicaenSql\Service\SQL;

trait RunSQLServiceAwareTrait
{
    /**
     * @var RunSQLService
     */
    protected $runSQLService;

    /**
     * @param RunSQLService $runSQLService
     * @return self
     */
    public function setRunSQLService(RunSQLService $runSQLService)
    {
        $this->runSQLService = $runSQLService;

        return $this;
    }
}