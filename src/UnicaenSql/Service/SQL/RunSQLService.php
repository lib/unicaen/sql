<?php

namespace UnicaenSql\Service\SQL;

use Doctrine\DBAL\Connection;
use Laminas\Log\LoggerAwareTrait;

/**
 * Service permettant d'exécuter des scripts SQL dans une base de données.
 *
 * @author Unicaen
 */
class RunSQLService
{
    use LoggerAwareTrait;

    /**
     * Exécute le script spécifié par son chemin.
     *
     * NB: Les requêtes doivent pouvoir être extraites afin de les exécuter une par une. Pour cela, vous devez donc
     * terminer chaque requête par une nouvelle ligne ne contenant que le caractère '/'.
     *
     * Exemple de script Oracle acceptable :
     * <code>
     *      insert into API_LOG (ID, REQ_URI, REQ_START_DATE, REQ_END_DATE, REQ_STATUS, REQ_RESPONSE, REQ_TABLE)
     *          select API_LOG_ID_SEQ.nextval, 'http://xyz.xx', sysdate, sysdate, 'test', 'hello!!', 'TEST' from dual
     *      /
     *
     *      declare
     *          d date;
     *      begin
     *          select sysdate into d from dual;
     *          insert into API_LOG(ID, REQ_URI, REQ_START_DATE, REQ_END_DATE, REQ_STATUS, REQ_RESPONSE, REQ_TABLE)
     *              select API_LOG_ID_SEQ.nextval, 'http://xyz.xx', d, d, 'test', 'hello/bonjour!!', 'TEST' from dual;
     *      end;
     *      /
     *
     *      create index TEST_index on FICHIER (HISTO_MODIFICATION ASC)
     *      /
     *      drop index TEST_index
     *      /
     * </code>
     *
     * @param string $path Chemin absolu d'un script SQL
     * @param Connection $conn Connexion Doctrine à la base de données
     * @param string|null $logFilepath Chemin du fichier de log à produire
     *
     * @return RunSQLResult
     */
    public function runSQLScript(string $path, Connection $conn, string $logFilepath = null): RunSQLResult
    {
        $process = new RunSQLProcess();
        $process
            ->setScriptPath($path)
            ->setConnection($conn)
            ->setLogFilePath($logFilepath)
            ->setLogger($this->logger);

        return $process->executeScript();
    }

    /**
     * Exécute une requête.
     *
     * @param string $query Requête à exécuter, ex: "begin DBMS_MVIEW.REFRESH('MV_RECHERCHE_THESE'); end;".
     * @param Connection $conn Connexion à la base de données
     * @param string|null $logFilepath Chemin du fichier de log à produire
     *
     * @return RunSQLResult
     */
    public function runSQLQuery(string $query, Connection $conn, string $logFilepath = null): RunSQLResult
    {
        $process = new RunSQLProcess();
        $process
            ->setConnection($conn)
            ->setLogFilePath($logFilepath)
            ->setLogger($this->logger);

        return $process->executeQuery($query);
    }
}