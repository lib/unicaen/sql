<?php

namespace UnicaenSql\Service\SQL;

use Exception;

/**
 * Interface décrivant le résultat de l'exécution d'un processus inconnu quelconque, çàd :
 * - un témoin de réussite ou non
 * - des logs d'exécution
 * - une exception éventuelle en cas d'erreur.
 * - une date de début d'exécution
 * - une date de fin d'exécution
 * - le calcul de la durée d'exécution
 *
 * @author Unicaen
 */
interface ProcessResultInterface
{
    /**
     * Retourne les logs.
     *
     * @return string
     */
    public function getLog();

    /**
     * Retourne le booléen indiquant si l'exécution est couronnée de succès ou non.
     *
     * @return bool
     */
    public function isSuccess();

    /**
     * Positionne le booléen indiquant si l'exécution est couronnée de succès ou non.
     *
     * @param bool $success
     * @return self
     */
    public function setIsSuccess($success = true);

    /**
     * @return float
     */
    public function getStartMicrotime();

    /**
     * @param float|null $startMicrotime
     * @return self
     */
    public function setStartMicrotime($startMicrotime = null);

    /**
     * @return float
     */
    public function getEndMicrotime();

    /**
     * @param float|null $endMicrotime
     * @return self
     */
    public function setEndMicrotime($endMicrotime = null);

    /**
     * @return float
     */
    public function getDurationInSec();

    /**
     * Retourne l'éventuelle exception rencontrée lors de l'exécution.
     *
     * @return null|Exception
     */
    public function getException();

    /**
     * Renseigne l'exception rencontrée lors de l'exécution.
     *
     * @param Exception $exception
     * @return self
     */
    public function setException(Exception $exception);
}
