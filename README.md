Module unicaen/sql
==================

Ce module a vocation à rassembler des choses utiles concernant le SQL.


Installation
------------

```bash
composer require unicaen/sql
```


Lignes de commandes
-------------------

- `run-sql-script --path= [--logfile=] [--connection=]` : Exécuter un script SQL
- `run-sql-query --sql= [--logfile=] [--connection=]` : Exécuter une requête SQL


Services
--------

- [`\UnicaenSql\Service\SQL\RunSQLProcess`](src/UnicaenSql/Service/SQL/RunSQLProcess.php) : lancement d'une instruction 
  ou d'un script SQL avec log détaillé. 
