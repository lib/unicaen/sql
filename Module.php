<?php

namespace UnicaenSql;

use Laminas\Config\Factory as ConfigFactory;
use Laminas\ModuleManager\Feature\ConfigProviderInterface;

class Module implements ConfigProviderInterface
{
    public function getConfig(): array
    {
        return ConfigFactory::fromFile(__DIR__ . '/config/module.config.php');
    }

    public function getAutoloaderConfig(): array
    {
        return array(
            'Laminas\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConsoleUsage(): array
    {
        return [
            'run-sql-script --path= [--logfile=] [--connection=]' => "Exécuter un script SQL",
            ['--path',       "Requis. Chemin vers le script SQL à exécuter."],
            ['--logfile',    "Facultatif. Chemin du fichier des logs d'exécution du script. Par défaut, il est généré."],
            ['--connection', "Facultatif. Identifiant de la connexion Doctrine. Par défaut : 'orm_default'."],

            'run-sql-query --sql= [--logfile=] [--connection=]' => "Exécuter une requête SQL",
            ['--sql',        "Requis. Requête SQL à exécuter. Ex: \"begin DBMS_MVIEW.REFRESH('MV_RECHERCHE_THESE'); end;\"."],
            ['--logfile',    "Facultatif. Chemin du fichier des logs d'exécution. Par défaut, il est généré."],
            ['--connection', "Facultatif. Identifiant de la connexion Doctrine. Par défaut : 'orm_default'."],
        ];
    }
}
